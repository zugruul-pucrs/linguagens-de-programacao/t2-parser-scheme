package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.SchemeGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSchemeParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_INT", "RULE_ARITHMETIC_OPERATORS", "RULE_LOGICAL_OPERATORS", "RULE_RELATIONAL_OPERATORS", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'('", "'define'", "')'", "'lambda'", "'if'", "'(print'", "'arr'", "'vector '"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_RELATIONAL_OPERATORS=8;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int RULE_LOGICAL_OPERATORS=7;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=9;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=10;
    public static final int RULE_ARITHMETIC_OPERATORS=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSchemeParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSchemeParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSchemeParser.tokenNames; }
    public String getGrammarFileName() { return "InternalScheme.g"; }



     	private SchemeGrammarAccess grammarAccess;

        public InternalSchemeParser(TokenStream input, SchemeGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected SchemeGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalScheme.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalScheme.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalScheme.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalScheme.g:71:1: ruleModel returns [EObject current=null] : ( (lv_commands_0_0= ruleCommand ) )* ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_commands_0_0 = null;



        	enterRule();

        try {
            // InternalScheme.g:77:2: ( ( (lv_commands_0_0= ruleCommand ) )* )
            // InternalScheme.g:78:2: ( (lv_commands_0_0= ruleCommand ) )*
            {
            // InternalScheme.g:78:2: ( (lv_commands_0_0= ruleCommand ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14||LA1_0==19) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalScheme.g:79:3: (lv_commands_0_0= ruleCommand )
            	    {
            	    // InternalScheme.g:79:3: (lv_commands_0_0= ruleCommand )
            	    // InternalScheme.g:80:4: lv_commands_0_0= ruleCommand
            	    {

            	    				newCompositeNode(grammarAccess.getModelAccess().getCommandsCommandParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_commands_0_0=ruleCommand();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getModelRule());
            	    				}
            	    				add(
            	    					current,
            	    					"commands",
            	    					lv_commands_0_0,
            	    					"org.xtext.example.mydsl.Scheme.Command");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleCommand"
    // InternalScheme.g:100:1: entryRuleCommand returns [EObject current=null] : iv_ruleCommand= ruleCommand EOF ;
    public final EObject entryRuleCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommand = null;


        try {
            // InternalScheme.g:100:48: (iv_ruleCommand= ruleCommand EOF )
            // InternalScheme.g:101:2: iv_ruleCommand= ruleCommand EOF
            {
             newCompositeNode(grammarAccess.getCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommand=ruleCommand();

            state._fsp--;

             current =iv_ruleCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // InternalScheme.g:107:1: ruleCommand returns [EObject current=null] : (this_Define_0= ruleDefine | this_Lambda_1= ruleLambda | this_Operation_2= ruleOperation | this_Conditional_3= ruleConditional | this_Print_4= rulePrint | this_Lista_5= ruleLista ) ;
    public final EObject ruleCommand() throws RecognitionException {
        EObject current = null;

        EObject this_Define_0 = null;

        EObject this_Lambda_1 = null;

        EObject this_Operation_2 = null;

        EObject this_Conditional_3 = null;

        EObject this_Print_4 = null;

        EObject this_Lista_5 = null;



        	enterRule();

        try {
            // InternalScheme.g:113:2: ( (this_Define_0= ruleDefine | this_Lambda_1= ruleLambda | this_Operation_2= ruleOperation | this_Conditional_3= ruleConditional | this_Print_4= rulePrint | this_Lista_5= ruleLista ) )
            // InternalScheme.g:114:2: (this_Define_0= ruleDefine | this_Lambda_1= ruleLambda | this_Operation_2= ruleOperation | this_Conditional_3= ruleConditional | this_Print_4= rulePrint | this_Lista_5= ruleLista )
            {
            // InternalScheme.g:114:2: (this_Define_0= ruleDefine | this_Lambda_1= ruleLambda | this_Operation_2= ruleOperation | this_Conditional_3= ruleConditional | this_Print_4= rulePrint | this_Lista_5= ruleLista )
            int alt2=6;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                switch ( input.LA(2) ) {
                case 15:
                    {
                    int LA2_3 = input.LA(3);

                    if ( (LA2_3==RULE_STRING) ) {
                        alt2=1;
                    }
                    else if ( (LA2_3==20) ) {
                        alt2=6;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 3, input);

                        throw nvae;
                    }
                    }
                    break;
                case 18:
                    {
                    alt2=4;
                    }
                    break;
                case 17:
                    {
                    alt2=2;
                    }
                    break;
                case RULE_ARITHMETIC_OPERATORS:
                case RULE_LOGICAL_OPERATORS:
                case RULE_RELATIONAL_OPERATORS:
                    {
                    alt2=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }

            }
            else if ( (LA2_0==19) ) {
                alt2=5;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalScheme.g:115:3: this_Define_0= ruleDefine
                    {

                    			newCompositeNode(grammarAccess.getCommandAccess().getDefineParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Define_0=ruleDefine();

                    state._fsp--;


                    			current = this_Define_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalScheme.g:124:3: this_Lambda_1= ruleLambda
                    {

                    			newCompositeNode(grammarAccess.getCommandAccess().getLambdaParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Lambda_1=ruleLambda();

                    state._fsp--;


                    			current = this_Lambda_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalScheme.g:133:3: this_Operation_2= ruleOperation
                    {

                    			newCompositeNode(grammarAccess.getCommandAccess().getOperationParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Operation_2=ruleOperation();

                    state._fsp--;


                    			current = this_Operation_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalScheme.g:142:3: this_Conditional_3= ruleConditional
                    {

                    			newCompositeNode(grammarAccess.getCommandAccess().getConditionalParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Conditional_3=ruleConditional();

                    state._fsp--;


                    			current = this_Conditional_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalScheme.g:151:3: this_Print_4= rulePrint
                    {

                    			newCompositeNode(grammarAccess.getCommandAccess().getPrintParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_Print_4=rulePrint();

                    state._fsp--;


                    			current = this_Print_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalScheme.g:160:3: this_Lista_5= ruleLista
                    {

                    			newCompositeNode(grammarAccess.getCommandAccess().getListaParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Lista_5=ruleLista();

                    state._fsp--;


                    			current = this_Lista_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleDefine"
    // InternalScheme.g:172:1: entryRuleDefine returns [EObject current=null] : iv_ruleDefine= ruleDefine EOF ;
    public final EObject entryRuleDefine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefine = null;


        try {
            // InternalScheme.g:172:47: (iv_ruleDefine= ruleDefine EOF )
            // InternalScheme.g:173:2: iv_ruleDefine= ruleDefine EOF
            {
             newCompositeNode(grammarAccess.getDefineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefine=ruleDefine();

            state._fsp--;

             current =iv_ruleDefine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefine"


    // $ANTLR start "ruleDefine"
    // InternalScheme.g:179:1: ruleDefine returns [EObject current=null] : (otherlv_0= '(' otherlv_1= 'define' ( (lv_varUm_2_0= RULE_STRING ) ) ( (lv_varValue_3_0= RULE_INT ) ) otherlv_4= ')' ) ;
    public final EObject ruleDefine() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_varUm_2_0=null;
        Token lv_varValue_3_0=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalScheme.g:185:2: ( (otherlv_0= '(' otherlv_1= 'define' ( (lv_varUm_2_0= RULE_STRING ) ) ( (lv_varValue_3_0= RULE_INT ) ) otherlv_4= ')' ) )
            // InternalScheme.g:186:2: (otherlv_0= '(' otherlv_1= 'define' ( (lv_varUm_2_0= RULE_STRING ) ) ( (lv_varValue_3_0= RULE_INT ) ) otherlv_4= ')' )
            {
            // InternalScheme.g:186:2: (otherlv_0= '(' otherlv_1= 'define' ( (lv_varUm_2_0= RULE_STRING ) ) ( (lv_varValue_3_0= RULE_INT ) ) otherlv_4= ')' )
            // InternalScheme.g:187:3: otherlv_0= '(' otherlv_1= 'define' ( (lv_varUm_2_0= RULE_STRING ) ) ( (lv_varValue_3_0= RULE_INT ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getDefineAccess().getLeftParenthesisKeyword_0());
            		
            otherlv_1=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getDefineAccess().getDefineKeyword_1());
            		
            // InternalScheme.g:195:3: ( (lv_varUm_2_0= RULE_STRING ) )
            // InternalScheme.g:196:4: (lv_varUm_2_0= RULE_STRING )
            {
            // InternalScheme.g:196:4: (lv_varUm_2_0= RULE_STRING )
            // InternalScheme.g:197:5: lv_varUm_2_0= RULE_STRING
            {
            lv_varUm_2_0=(Token)match(input,RULE_STRING,FOLLOW_6); 

            					newLeafNode(lv_varUm_2_0, grammarAccess.getDefineAccess().getVarUmSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDefineRule());
            					}
            					setWithLastConsumed(
            						current,
            						"varUm",
            						lv_varUm_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalScheme.g:213:3: ( (lv_varValue_3_0= RULE_INT ) )
            // InternalScheme.g:214:4: (lv_varValue_3_0= RULE_INT )
            {
            // InternalScheme.g:214:4: (lv_varValue_3_0= RULE_INT )
            // InternalScheme.g:215:5: lv_varValue_3_0= RULE_INT
            {
            lv_varValue_3_0=(Token)match(input,RULE_INT,FOLLOW_7); 

            					newLeafNode(lv_varValue_3_0, grammarAccess.getDefineAccess().getVarValueINTTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDefineRule());
            					}
            					setWithLastConsumed(
            						current,
            						"varValue",
            						lv_varValue_3_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getDefineAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefine"


    // $ANTLR start "entryRuleLambda"
    // InternalScheme.g:239:1: entryRuleLambda returns [EObject current=null] : iv_ruleLambda= ruleLambda EOF ;
    public final EObject entryRuleLambda() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLambda = null;


        try {
            // InternalScheme.g:239:47: (iv_ruleLambda= ruleLambda EOF )
            // InternalScheme.g:240:2: iv_ruleLambda= ruleLambda EOF
            {
             newCompositeNode(grammarAccess.getLambdaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLambda=ruleLambda();

            state._fsp--;

             current =iv_ruleLambda; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLambda"


    // $ANTLR start "ruleLambda"
    // InternalScheme.g:246:1: ruleLambda returns [EObject current=null] : (otherlv_0= '(' otherlv_1= 'lambda' otherlv_2= '(' ( (lv_param_3_0= ruleAny ) )+ otherlv_4= ')' otherlv_5= ')' ) ;
    public final EObject ruleLambda() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_param_3_0 = null;



        	enterRule();

        try {
            // InternalScheme.g:252:2: ( (otherlv_0= '(' otherlv_1= 'lambda' otherlv_2= '(' ( (lv_param_3_0= ruleAny ) )+ otherlv_4= ')' otherlv_5= ')' ) )
            // InternalScheme.g:253:2: (otherlv_0= '(' otherlv_1= 'lambda' otherlv_2= '(' ( (lv_param_3_0= ruleAny ) )+ otherlv_4= ')' otherlv_5= ')' )
            {
            // InternalScheme.g:253:2: (otherlv_0= '(' otherlv_1= 'lambda' otherlv_2= '(' ( (lv_param_3_0= ruleAny ) )+ otherlv_4= ')' otherlv_5= ')' )
            // InternalScheme.g:254:3: otherlv_0= '(' otherlv_1= 'lambda' otherlv_2= '(' ( (lv_param_3_0= ruleAny ) )+ otherlv_4= ')' otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_0, grammarAccess.getLambdaAccess().getLeftParenthesisKeyword_0());
            		
            otherlv_1=(Token)match(input,17,FOLLOW_9); 

            			newLeafNode(otherlv_1, grammarAccess.getLambdaAccess().getLambdaKeyword_1());
            		
            otherlv_2=(Token)match(input,14,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getLambdaAccess().getLeftParenthesisKeyword_2());
            		
            // InternalScheme.g:266:3: ( (lv_param_3_0= ruleAny ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=RULE_STRING && LA3_0<=RULE_INT)||LA3_0==RULE_ID||LA3_0==14) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalScheme.g:267:4: (lv_param_3_0= ruleAny )
            	    {
            	    // InternalScheme.g:267:4: (lv_param_3_0= ruleAny )
            	    // InternalScheme.g:268:5: lv_param_3_0= ruleAny
            	    {

            	    					newCompositeNode(grammarAccess.getLambdaAccess().getParamAnyParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_param_3_0=ruleAny();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getLambdaRule());
            	    					}
            	    					add(
            	    						current,
            	    						"param",
            	    						lv_param_3_0,
            	    						"org.xtext.example.mydsl.Scheme.Any");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            otherlv_4=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_4, grammarAccess.getLambdaAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getLambdaAccess().getRightParenthesisKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLambda"


    // $ANTLR start "entryRuleOperation"
    // InternalScheme.g:297:1: entryRuleOperation returns [EObject current=null] : iv_ruleOperation= ruleOperation EOF ;
    public final EObject entryRuleOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperation = null;


        try {
            // InternalScheme.g:297:50: (iv_ruleOperation= ruleOperation EOF )
            // InternalScheme.g:298:2: iv_ruleOperation= ruleOperation EOF
            {
             newCompositeNode(grammarAccess.getOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperation=ruleOperation();

            state._fsp--;

             current =iv_ruleOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperation"


    // $ANTLR start "ruleOperation"
    // InternalScheme.g:304:1: ruleOperation returns [EObject current=null] : (otherlv_0= '(' ( ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) ) | ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) ) | ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) ) ) ( (lv_valueUm_4_0= RULE_INT ) ) ( (lv_valueDois_5_0= RULE_INT ) ) otherlv_6= ')' ) ;
    public final EObject ruleOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_arit_op_1_0=null;
        Token lv_log_op_2_0=null;
        Token lv_rel_op_3_0=null;
        Token lv_valueUm_4_0=null;
        Token lv_valueDois_5_0=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalScheme.g:310:2: ( (otherlv_0= '(' ( ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) ) | ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) ) | ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) ) ) ( (lv_valueUm_4_0= RULE_INT ) ) ( (lv_valueDois_5_0= RULE_INT ) ) otherlv_6= ')' ) )
            // InternalScheme.g:311:2: (otherlv_0= '(' ( ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) ) | ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) ) | ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) ) ) ( (lv_valueUm_4_0= RULE_INT ) ) ( (lv_valueDois_5_0= RULE_INT ) ) otherlv_6= ')' )
            {
            // InternalScheme.g:311:2: (otherlv_0= '(' ( ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) ) | ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) ) | ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) ) ) ( (lv_valueUm_4_0= RULE_INT ) ) ( (lv_valueDois_5_0= RULE_INT ) ) otherlv_6= ')' )
            // InternalScheme.g:312:3: otherlv_0= '(' ( ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) ) | ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) ) | ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) ) ) ( (lv_valueUm_4_0= RULE_INT ) ) ( (lv_valueDois_5_0= RULE_INT ) ) otherlv_6= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getOperationAccess().getLeftParenthesisKeyword_0());
            		
            // InternalScheme.g:316:3: ( ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) ) | ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) ) | ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case RULE_ARITHMETIC_OPERATORS:
                {
                alt4=1;
                }
                break;
            case RULE_LOGICAL_OPERATORS:
                {
                alt4=2;
                }
                break;
            case RULE_RELATIONAL_OPERATORS:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalScheme.g:317:4: ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) )
                    {
                    // InternalScheme.g:317:4: ( (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS ) )
                    // InternalScheme.g:318:5: (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS )
                    {
                    // InternalScheme.g:318:5: (lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS )
                    // InternalScheme.g:319:6: lv_arit_op_1_0= RULE_ARITHMETIC_OPERATORS
                    {
                    lv_arit_op_1_0=(Token)match(input,RULE_ARITHMETIC_OPERATORS,FOLLOW_6); 

                    						newLeafNode(lv_arit_op_1_0, grammarAccess.getOperationAccess().getArit_opARITHMETIC_OPERATORSTerminalRuleCall_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOperationRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"arit_op",
                    							lv_arit_op_1_0,
                    							"org.xtext.example.mydsl.Scheme.ARITHMETIC_OPERATORS");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalScheme.g:336:4: ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) )
                    {
                    // InternalScheme.g:336:4: ( (lv_log_op_2_0= RULE_LOGICAL_OPERATORS ) )
                    // InternalScheme.g:337:5: (lv_log_op_2_0= RULE_LOGICAL_OPERATORS )
                    {
                    // InternalScheme.g:337:5: (lv_log_op_2_0= RULE_LOGICAL_OPERATORS )
                    // InternalScheme.g:338:6: lv_log_op_2_0= RULE_LOGICAL_OPERATORS
                    {
                    lv_log_op_2_0=(Token)match(input,RULE_LOGICAL_OPERATORS,FOLLOW_6); 

                    						newLeafNode(lv_log_op_2_0, grammarAccess.getOperationAccess().getLog_opLOGICAL_OPERATORSTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOperationRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"log_op",
                    							lv_log_op_2_0,
                    							"org.xtext.example.mydsl.Scheme.LOGICAL_OPERATORS");
                    					

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalScheme.g:355:4: ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) )
                    {
                    // InternalScheme.g:355:4: ( (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS ) )
                    // InternalScheme.g:356:5: (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS )
                    {
                    // InternalScheme.g:356:5: (lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS )
                    // InternalScheme.g:357:6: lv_rel_op_3_0= RULE_RELATIONAL_OPERATORS
                    {
                    lv_rel_op_3_0=(Token)match(input,RULE_RELATIONAL_OPERATORS,FOLLOW_6); 

                    						newLeafNode(lv_rel_op_3_0, grammarAccess.getOperationAccess().getRel_opRELATIONAL_OPERATORSTerminalRuleCall_1_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOperationRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"rel_op",
                    							lv_rel_op_3_0,
                    							"org.xtext.example.mydsl.Scheme.RELATIONAL_OPERATORS");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalScheme.g:374:3: ( (lv_valueUm_4_0= RULE_INT ) )
            // InternalScheme.g:375:4: (lv_valueUm_4_0= RULE_INT )
            {
            // InternalScheme.g:375:4: (lv_valueUm_4_0= RULE_INT )
            // InternalScheme.g:376:5: lv_valueUm_4_0= RULE_INT
            {
            lv_valueUm_4_0=(Token)match(input,RULE_INT,FOLLOW_6); 

            					newLeafNode(lv_valueUm_4_0, grammarAccess.getOperationAccess().getValueUmINTTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOperationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"valueUm",
            						lv_valueUm_4_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            // InternalScheme.g:392:3: ( (lv_valueDois_5_0= RULE_INT ) )
            // InternalScheme.g:393:4: (lv_valueDois_5_0= RULE_INT )
            {
            // InternalScheme.g:393:4: (lv_valueDois_5_0= RULE_INT )
            // InternalScheme.g:394:5: lv_valueDois_5_0= RULE_INT
            {
            lv_valueDois_5_0=(Token)match(input,RULE_INT,FOLLOW_7); 

            					newLeafNode(lv_valueDois_5_0, grammarAccess.getOperationAccess().getValueDoisINTTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOperationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"valueDois",
            						lv_valueDois_5_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_6=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getOperationAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperation"


    // $ANTLR start "entryRuleConditional"
    // InternalScheme.g:418:1: entryRuleConditional returns [EObject current=null] : iv_ruleConditional= ruleConditional EOF ;
    public final EObject entryRuleConditional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditional = null;


        try {
            // InternalScheme.g:418:52: (iv_ruleConditional= ruleConditional EOF )
            // InternalScheme.g:419:2: iv_ruleConditional= ruleConditional EOF
            {
             newCompositeNode(grammarAccess.getConditionalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditional=ruleConditional();

            state._fsp--;

             current =iv_ruleConditional; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditional"


    // $ANTLR start "ruleConditional"
    // InternalScheme.g:425:1: ruleConditional returns [EObject current=null] : (otherlv_0= '(' otherlv_1= 'if' ( (lv_cond_2_0= ruleAny ) ) ( (lv_truthy_3_0= ruleAny ) ) ( (lv_falsy_4_0= ruleAny ) ) otherlv_5= ')' ) ;
    public final EObject ruleConditional() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_5=null;
        EObject lv_cond_2_0 = null;

        EObject lv_truthy_3_0 = null;

        EObject lv_falsy_4_0 = null;



        	enterRule();

        try {
            // InternalScheme.g:431:2: ( (otherlv_0= '(' otherlv_1= 'if' ( (lv_cond_2_0= ruleAny ) ) ( (lv_truthy_3_0= ruleAny ) ) ( (lv_falsy_4_0= ruleAny ) ) otherlv_5= ')' ) )
            // InternalScheme.g:432:2: (otherlv_0= '(' otherlv_1= 'if' ( (lv_cond_2_0= ruleAny ) ) ( (lv_truthy_3_0= ruleAny ) ) ( (lv_falsy_4_0= ruleAny ) ) otherlv_5= ')' )
            {
            // InternalScheme.g:432:2: (otherlv_0= '(' otherlv_1= 'if' ( (lv_cond_2_0= ruleAny ) ) ( (lv_truthy_3_0= ruleAny ) ) ( (lv_falsy_4_0= ruleAny ) ) otherlv_5= ')' )
            // InternalScheme.g:433:3: otherlv_0= '(' otherlv_1= 'if' ( (lv_cond_2_0= ruleAny ) ) ( (lv_truthy_3_0= ruleAny ) ) ( (lv_falsy_4_0= ruleAny ) ) otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getConditionalAccess().getLeftParenthesisKeyword_0());
            		
            otherlv_1=(Token)match(input,18,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getConditionalAccess().getIfKeyword_1());
            		
            // InternalScheme.g:441:3: ( (lv_cond_2_0= ruleAny ) )
            // InternalScheme.g:442:4: (lv_cond_2_0= ruleAny )
            {
            // InternalScheme.g:442:4: (lv_cond_2_0= ruleAny )
            // InternalScheme.g:443:5: lv_cond_2_0= ruleAny
            {

            					newCompositeNode(grammarAccess.getConditionalAccess().getCondAnyParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_10);
            lv_cond_2_0=ruleAny();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConditionalRule());
            					}
            					set(
            						current,
            						"cond",
            						lv_cond_2_0,
            						"org.xtext.example.mydsl.Scheme.Any");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalScheme.g:460:3: ( (lv_truthy_3_0= ruleAny ) )
            // InternalScheme.g:461:4: (lv_truthy_3_0= ruleAny )
            {
            // InternalScheme.g:461:4: (lv_truthy_3_0= ruleAny )
            // InternalScheme.g:462:5: lv_truthy_3_0= ruleAny
            {

            					newCompositeNode(grammarAccess.getConditionalAccess().getTruthyAnyParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_10);
            lv_truthy_3_0=ruleAny();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConditionalRule());
            					}
            					set(
            						current,
            						"truthy",
            						lv_truthy_3_0,
            						"org.xtext.example.mydsl.Scheme.Any");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalScheme.g:479:3: ( (lv_falsy_4_0= ruleAny ) )
            // InternalScheme.g:480:4: (lv_falsy_4_0= ruleAny )
            {
            // InternalScheme.g:480:4: (lv_falsy_4_0= ruleAny )
            // InternalScheme.g:481:5: lv_falsy_4_0= ruleAny
            {

            					newCompositeNode(grammarAccess.getConditionalAccess().getFalsyAnyParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_falsy_4_0=ruleAny();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConditionalRule());
            					}
            					set(
            						current,
            						"falsy",
            						lv_falsy_4_0,
            						"org.xtext.example.mydsl.Scheme.Any");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getConditionalAccess().getRightParenthesisKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditional"


    // $ANTLR start "entryRulePrint"
    // InternalScheme.g:506:1: entryRulePrint returns [EObject current=null] : iv_rulePrint= rulePrint EOF ;
    public final EObject entryRulePrint() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrint = null;


        try {
            // InternalScheme.g:506:46: (iv_rulePrint= rulePrint EOF )
            // InternalScheme.g:507:2: iv_rulePrint= rulePrint EOF
            {
             newCompositeNode(grammarAccess.getPrintRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrint=rulePrint();

            state._fsp--;

             current =iv_rulePrint; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrint"


    // $ANTLR start "rulePrint"
    // InternalScheme.g:513:1: rulePrint returns [EObject current=null] : (otherlv_0= '(print' ( (lv_string_1_0= RULE_STRING ) ) otherlv_2= ')' ) ;
    public final EObject rulePrint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_string_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalScheme.g:519:2: ( (otherlv_0= '(print' ( (lv_string_1_0= RULE_STRING ) ) otherlv_2= ')' ) )
            // InternalScheme.g:520:2: (otherlv_0= '(print' ( (lv_string_1_0= RULE_STRING ) ) otherlv_2= ')' )
            {
            // InternalScheme.g:520:2: (otherlv_0= '(print' ( (lv_string_1_0= RULE_STRING ) ) otherlv_2= ')' )
            // InternalScheme.g:521:3: otherlv_0= '(print' ( (lv_string_1_0= RULE_STRING ) ) otherlv_2= ')'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getPrintAccess().getPrintKeyword_0());
            		
            // InternalScheme.g:525:3: ( (lv_string_1_0= RULE_STRING ) )
            // InternalScheme.g:526:4: (lv_string_1_0= RULE_STRING )
            {
            // InternalScheme.g:526:4: (lv_string_1_0= RULE_STRING )
            // InternalScheme.g:527:5: lv_string_1_0= RULE_STRING
            {
            lv_string_1_0=(Token)match(input,RULE_STRING,FOLLOW_7); 

            					newLeafNode(lv_string_1_0, grammarAccess.getPrintAccess().getStringSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPrintRule());
            					}
            					setWithLastConsumed(
            						current,
            						"string",
            						lv_string_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getPrintAccess().getRightParenthesisKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrint"


    // $ANTLR start "entryRuleLista"
    // InternalScheme.g:551:1: entryRuleLista returns [EObject current=null] : iv_ruleLista= ruleLista EOF ;
    public final EObject entryRuleLista() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLista = null;


        try {
            // InternalScheme.g:551:46: (iv_ruleLista= ruleLista EOF )
            // InternalScheme.g:552:2: iv_ruleLista= ruleLista EOF
            {
             newCompositeNode(grammarAccess.getListaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLista=ruleLista();

            state._fsp--;

             current =iv_ruleLista; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLista"


    // $ANTLR start "ruleLista"
    // InternalScheme.g:558:1: ruleLista returns [EObject current=null] : (otherlv_0= '(' otherlv_1= 'define' otherlv_2= 'arr' otherlv_3= '(' otherlv_4= 'vector ' ( (lv_numeros_5_0= RULE_INT ) )+ otherlv_6= ')' otherlv_7= ')' ) ;
    public final EObject ruleLista() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_numeros_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;


        	enterRule();

        try {
            // InternalScheme.g:564:2: ( (otherlv_0= '(' otherlv_1= 'define' otherlv_2= 'arr' otherlv_3= '(' otherlv_4= 'vector ' ( (lv_numeros_5_0= RULE_INT ) )+ otherlv_6= ')' otherlv_7= ')' ) )
            // InternalScheme.g:565:2: (otherlv_0= '(' otherlv_1= 'define' otherlv_2= 'arr' otherlv_3= '(' otherlv_4= 'vector ' ( (lv_numeros_5_0= RULE_INT ) )+ otherlv_6= ')' otherlv_7= ')' )
            {
            // InternalScheme.g:565:2: (otherlv_0= '(' otherlv_1= 'define' otherlv_2= 'arr' otherlv_3= '(' otherlv_4= 'vector ' ( (lv_numeros_5_0= RULE_INT ) )+ otherlv_6= ')' otherlv_7= ')' )
            // InternalScheme.g:566:3: otherlv_0= '(' otherlv_1= 'define' otherlv_2= 'arr' otherlv_3= '(' otherlv_4= 'vector ' ( (lv_numeros_5_0= RULE_INT ) )+ otherlv_6= ')' otherlv_7= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getListaAccess().getLeftParenthesisKeyword_0());
            		
            otherlv_1=(Token)match(input,15,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getListaAccess().getDefineKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getListaAccess().getArrKeyword_2());
            		
            otherlv_3=(Token)match(input,14,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getListaAccess().getLeftParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,21,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getListaAccess().getVectorKeyword_4());
            		
            // InternalScheme.g:586:3: ( (lv_numeros_5_0= RULE_INT ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_INT) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalScheme.g:587:4: (lv_numeros_5_0= RULE_INT )
            	    {
            	    // InternalScheme.g:587:4: (lv_numeros_5_0= RULE_INT )
            	    // InternalScheme.g:588:5: lv_numeros_5_0= RULE_INT
            	    {
            	    lv_numeros_5_0=(Token)match(input,RULE_INT,FOLLOW_16); 

            	    					newLeafNode(lv_numeros_5_0, grammarAccess.getListaAccess().getNumerosINTTerminalRuleCall_5_0());
            	    				

            	    					if (current==null) {
            	    						current = createModelElement(grammarAccess.getListaRule());
            	    					}
            	    					addWithLastConsumed(
            	    						current,
            	    						"numeros",
            	    						lv_numeros_5_0,
            	    						"org.eclipse.xtext.common.Terminals.INT");
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            otherlv_6=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_6, grammarAccess.getListaAccess().getRightParenthesisKeyword_6());
            		
            otherlv_7=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getListaAccess().getRightParenthesisKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLista"


    // $ANTLR start "entryRuleAny"
    // InternalScheme.g:616:1: entryRuleAny returns [EObject current=null] : iv_ruleAny= ruleAny EOF ;
    public final EObject entryRuleAny() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAny = null;


        try {
            // InternalScheme.g:616:44: (iv_ruleAny= ruleAny EOF )
            // InternalScheme.g:617:2: iv_ruleAny= ruleAny EOF
            {
             newCompositeNode(grammarAccess.getAnyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAny=ruleAny();

            state._fsp--;

             current =iv_ruleAny; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAny"


    // $ANTLR start "ruleAny"
    // InternalScheme.g:623:1: ruleAny returns [EObject current=null] : ( ( (lv_anyID_0_0= RULE_ID ) ) | ( (lv_anyInt_1_0= RULE_INT ) ) | ( (lv_anyString_2_0= RULE_STRING ) ) | ( (lv_anyDefine_3_0= ruleDefine ) ) | ( (lv_anyLambda_4_0= ruleLambda ) ) | ( (lv_anyOperation_5_0= ruleOperation ) ) | ( (lv_anyConditional_6_0= ruleConditional ) ) ) ;
    public final EObject ruleAny() throws RecognitionException {
        EObject current = null;

        Token lv_anyID_0_0=null;
        Token lv_anyInt_1_0=null;
        Token lv_anyString_2_0=null;
        EObject lv_anyDefine_3_0 = null;

        EObject lv_anyLambda_4_0 = null;

        EObject lv_anyOperation_5_0 = null;

        EObject lv_anyConditional_6_0 = null;



        	enterRule();

        try {
            // InternalScheme.g:629:2: ( ( ( (lv_anyID_0_0= RULE_ID ) ) | ( (lv_anyInt_1_0= RULE_INT ) ) | ( (lv_anyString_2_0= RULE_STRING ) ) | ( (lv_anyDefine_3_0= ruleDefine ) ) | ( (lv_anyLambda_4_0= ruleLambda ) ) | ( (lv_anyOperation_5_0= ruleOperation ) ) | ( (lv_anyConditional_6_0= ruleConditional ) ) ) )
            // InternalScheme.g:630:2: ( ( (lv_anyID_0_0= RULE_ID ) ) | ( (lv_anyInt_1_0= RULE_INT ) ) | ( (lv_anyString_2_0= RULE_STRING ) ) | ( (lv_anyDefine_3_0= ruleDefine ) ) | ( (lv_anyLambda_4_0= ruleLambda ) ) | ( (lv_anyOperation_5_0= ruleOperation ) ) | ( (lv_anyConditional_6_0= ruleConditional ) ) )
            {
            // InternalScheme.g:630:2: ( ( (lv_anyID_0_0= RULE_ID ) ) | ( (lv_anyInt_1_0= RULE_INT ) ) | ( (lv_anyString_2_0= RULE_STRING ) ) | ( (lv_anyDefine_3_0= ruleDefine ) ) | ( (lv_anyLambda_4_0= ruleLambda ) ) | ( (lv_anyOperation_5_0= ruleOperation ) ) | ( (lv_anyConditional_6_0= ruleConditional ) ) )
            int alt6=7;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt6=1;
                }
                break;
            case RULE_INT:
                {
                alt6=2;
                }
                break;
            case RULE_STRING:
                {
                alt6=3;
                }
                break;
            case 14:
                {
                switch ( input.LA(2) ) {
                case 15:
                    {
                    alt6=4;
                    }
                    break;
                case 18:
                    {
                    alt6=7;
                    }
                    break;
                case 17:
                    {
                    alt6=5;
                    }
                    break;
                case RULE_ARITHMETIC_OPERATORS:
                case RULE_LOGICAL_OPERATORS:
                case RULE_RELATIONAL_OPERATORS:
                    {
                    alt6=6;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 4, input);

                    throw nvae;
                }

                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalScheme.g:631:3: ( (lv_anyID_0_0= RULE_ID ) )
                    {
                    // InternalScheme.g:631:3: ( (lv_anyID_0_0= RULE_ID ) )
                    // InternalScheme.g:632:4: (lv_anyID_0_0= RULE_ID )
                    {
                    // InternalScheme.g:632:4: (lv_anyID_0_0= RULE_ID )
                    // InternalScheme.g:633:5: lv_anyID_0_0= RULE_ID
                    {
                    lv_anyID_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(lv_anyID_0_0, grammarAccess.getAnyAccess().getAnyIDIDTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAnyRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"anyID",
                    						lv_anyID_0_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalScheme.g:650:3: ( (lv_anyInt_1_0= RULE_INT ) )
                    {
                    // InternalScheme.g:650:3: ( (lv_anyInt_1_0= RULE_INT ) )
                    // InternalScheme.g:651:4: (lv_anyInt_1_0= RULE_INT )
                    {
                    // InternalScheme.g:651:4: (lv_anyInt_1_0= RULE_INT )
                    // InternalScheme.g:652:5: lv_anyInt_1_0= RULE_INT
                    {
                    lv_anyInt_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    					newLeafNode(lv_anyInt_1_0, grammarAccess.getAnyAccess().getAnyIntINTTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAnyRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"anyInt",
                    						lv_anyInt_1_0,
                    						"org.eclipse.xtext.common.Terminals.INT");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalScheme.g:669:3: ( (lv_anyString_2_0= RULE_STRING ) )
                    {
                    // InternalScheme.g:669:3: ( (lv_anyString_2_0= RULE_STRING ) )
                    // InternalScheme.g:670:4: (lv_anyString_2_0= RULE_STRING )
                    {
                    // InternalScheme.g:670:4: (lv_anyString_2_0= RULE_STRING )
                    // InternalScheme.g:671:5: lv_anyString_2_0= RULE_STRING
                    {
                    lv_anyString_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    					newLeafNode(lv_anyString_2_0, grammarAccess.getAnyAccess().getAnyStringSTRINGTerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAnyRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"anyString",
                    						lv_anyString_2_0,
                    						"org.eclipse.xtext.common.Terminals.STRING");
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalScheme.g:688:3: ( (lv_anyDefine_3_0= ruleDefine ) )
                    {
                    // InternalScheme.g:688:3: ( (lv_anyDefine_3_0= ruleDefine ) )
                    // InternalScheme.g:689:4: (lv_anyDefine_3_0= ruleDefine )
                    {
                    // InternalScheme.g:689:4: (lv_anyDefine_3_0= ruleDefine )
                    // InternalScheme.g:690:5: lv_anyDefine_3_0= ruleDefine
                    {

                    					newCompositeNode(grammarAccess.getAnyAccess().getAnyDefineDefineParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_anyDefine_3_0=ruleDefine();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAnyRule());
                    					}
                    					set(
                    						current,
                    						"anyDefine",
                    						lv_anyDefine_3_0,
                    						"org.xtext.example.mydsl.Scheme.Define");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalScheme.g:708:3: ( (lv_anyLambda_4_0= ruleLambda ) )
                    {
                    // InternalScheme.g:708:3: ( (lv_anyLambda_4_0= ruleLambda ) )
                    // InternalScheme.g:709:4: (lv_anyLambda_4_0= ruleLambda )
                    {
                    // InternalScheme.g:709:4: (lv_anyLambda_4_0= ruleLambda )
                    // InternalScheme.g:710:5: lv_anyLambda_4_0= ruleLambda
                    {

                    					newCompositeNode(grammarAccess.getAnyAccess().getAnyLambdaLambdaParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_anyLambda_4_0=ruleLambda();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAnyRule());
                    					}
                    					set(
                    						current,
                    						"anyLambda",
                    						lv_anyLambda_4_0,
                    						"org.xtext.example.mydsl.Scheme.Lambda");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalScheme.g:728:3: ( (lv_anyOperation_5_0= ruleOperation ) )
                    {
                    // InternalScheme.g:728:3: ( (lv_anyOperation_5_0= ruleOperation ) )
                    // InternalScheme.g:729:4: (lv_anyOperation_5_0= ruleOperation )
                    {
                    // InternalScheme.g:729:4: (lv_anyOperation_5_0= ruleOperation )
                    // InternalScheme.g:730:5: lv_anyOperation_5_0= ruleOperation
                    {

                    					newCompositeNode(grammarAccess.getAnyAccess().getAnyOperationOperationParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_anyOperation_5_0=ruleOperation();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAnyRule());
                    					}
                    					set(
                    						current,
                    						"anyOperation",
                    						lv_anyOperation_5_0,
                    						"org.xtext.example.mydsl.Scheme.Operation");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 7 :
                    // InternalScheme.g:748:3: ( (lv_anyConditional_6_0= ruleConditional ) )
                    {
                    // InternalScheme.g:748:3: ( (lv_anyConditional_6_0= ruleConditional ) )
                    // InternalScheme.g:749:4: (lv_anyConditional_6_0= ruleConditional )
                    {
                    // InternalScheme.g:749:4: (lv_anyConditional_6_0= ruleConditional )
                    // InternalScheme.g:750:5: lv_anyConditional_6_0= ruleConditional
                    {

                    					newCompositeNode(grammarAccess.getAnyAccess().getAnyConditionalConditionalParserRuleCall_6_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_anyConditional_6_0=ruleConditional();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAnyRule());
                    					}
                    					set(
                    						current,
                    						"anyConditional",
                    						lv_anyConditional_6_0,
                    						"org.xtext.example.mydsl.Scheme.Conditional");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAny"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000084002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000004230L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000014230L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000001C0L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000010020L});

}