/**
 * generated by Xtext 2.17.0
 */
package org.xtext.example.mydsl.scheme;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.scheme.SchemeFactory
 * @model kind="package"
 * @generated
 */
public interface SchemePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "scheme";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/example/mydsl/Scheme";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "scheme";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SchemePackage eINSTANCE = org.xtext.example.mydsl.scheme.impl.SchemePackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.ModelImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Commands</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__COMMANDS = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.CommandImpl <em>Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.CommandImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getCommand()
   * @generated
   */
  int COMMAND = 1;

  /**
   * The number of structural features of the '<em>Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.DefineImpl <em>Define</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.DefineImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getDefine()
   * @generated
   */
  int DEFINE = 2;

  /**
   * The feature id for the '<em><b>Var Um</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINE__VAR_UM = COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Var Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINE__VAR_VALUE = COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Define</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINE_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.LambdaImpl <em>Lambda</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.LambdaImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getLambda()
   * @generated
   */
  int LAMBDA = 3;

  /**
   * The feature id for the '<em><b>Param</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAMBDA__PARAM = COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Lambda</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAMBDA_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.OperationImpl <em>Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.OperationImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getOperation()
   * @generated
   */
  int OPERATION = 4;

  /**
   * The feature id for the '<em><b>Arit op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPERATION__ARIT_OP = COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Log op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPERATION__LOG_OP = COMMAND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Rel op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPERATION__REL_OP = COMMAND_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Value Um</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPERATION__VALUE_UM = COMMAND_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Value Dois</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPERATION__VALUE_DOIS = COMMAND_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPERATION_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.ConditionalImpl <em>Conditional</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.ConditionalImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getConditional()
   * @generated
   */
  int CONDITIONAL = 5;

  /**
   * The feature id for the '<em><b>Cond</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__COND = COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Truthy</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__TRUTHY = COMMAND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Falsy</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__FALSY = COMMAND_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Conditional</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.PrintImpl <em>Print</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.PrintImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getPrint()
   * @generated
   */
  int PRINT = 6;

  /**
   * The feature id for the '<em><b>String</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT__STRING = COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Print</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.ListaImpl <em>Lista</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.ListaImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getLista()
   * @generated
   */
  int LISTA = 7;

  /**
   * The feature id for the '<em><b>Numeros</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LISTA__NUMEROS = COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Lista</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LISTA_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.scheme.impl.AnyImpl <em>Any</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.scheme.impl.AnyImpl
   * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getAny()
   * @generated
   */
  int ANY = 8;

  /**
   * The feature id for the '<em><b>Any ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY__ANY_ID = 0;

  /**
   * The feature id for the '<em><b>Any Int</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY__ANY_INT = 1;

  /**
   * The feature id for the '<em><b>Any String</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY__ANY_STRING = 2;

  /**
   * The feature id for the '<em><b>Any Define</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY__ANY_DEFINE = 3;

  /**
   * The feature id for the '<em><b>Any Lambda</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY__ANY_LAMBDA = 4;

  /**
   * The feature id for the '<em><b>Any Operation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY__ANY_OPERATION = 5;

  /**
   * The feature id for the '<em><b>Any Conditional</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY__ANY_CONDITIONAL = 6;

  /**
   * The number of structural features of the '<em>Any</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY_FEATURE_COUNT = 7;


  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see org.xtext.example.mydsl.scheme.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.scheme.Model#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Commands</em>'.
   * @see org.xtext.example.mydsl.scheme.Model#getCommands()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Commands();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Command <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Command</em>'.
   * @see org.xtext.example.mydsl.scheme.Command
   * @generated
   */
  EClass getCommand();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Define <em>Define</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Define</em>'.
   * @see org.xtext.example.mydsl.scheme.Define
   * @generated
   */
  EClass getDefine();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Define#getVarUm <em>Var Um</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Var Um</em>'.
   * @see org.xtext.example.mydsl.scheme.Define#getVarUm()
   * @see #getDefine()
   * @generated
   */
  EAttribute getDefine_VarUm();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Define#getVarValue <em>Var Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Var Value</em>'.
   * @see org.xtext.example.mydsl.scheme.Define#getVarValue()
   * @see #getDefine()
   * @generated
   */
  EAttribute getDefine_VarValue();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Lambda <em>Lambda</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lambda</em>'.
   * @see org.xtext.example.mydsl.scheme.Lambda
   * @generated
   */
  EClass getLambda();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.scheme.Lambda#getParam <em>Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Param</em>'.
   * @see org.xtext.example.mydsl.scheme.Lambda#getParam()
   * @see #getLambda()
   * @generated
   */
  EReference getLambda_Param();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Operation <em>Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Operation</em>'.
   * @see org.xtext.example.mydsl.scheme.Operation
   * @generated
   */
  EClass getOperation();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Operation#getArit_op <em>Arit op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Arit op</em>'.
   * @see org.xtext.example.mydsl.scheme.Operation#getArit_op()
   * @see #getOperation()
   * @generated
   */
  EAttribute getOperation_Arit_op();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Operation#getLog_op <em>Log op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Log op</em>'.
   * @see org.xtext.example.mydsl.scheme.Operation#getLog_op()
   * @see #getOperation()
   * @generated
   */
  EAttribute getOperation_Log_op();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Operation#getRel_op <em>Rel op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Rel op</em>'.
   * @see org.xtext.example.mydsl.scheme.Operation#getRel_op()
   * @see #getOperation()
   * @generated
   */
  EAttribute getOperation_Rel_op();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Operation#getValueUm <em>Value Um</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value Um</em>'.
   * @see org.xtext.example.mydsl.scheme.Operation#getValueUm()
   * @see #getOperation()
   * @generated
   */
  EAttribute getOperation_ValueUm();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Operation#getValueDois <em>Value Dois</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value Dois</em>'.
   * @see org.xtext.example.mydsl.scheme.Operation#getValueDois()
   * @see #getOperation()
   * @generated
   */
  EAttribute getOperation_ValueDois();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Conditional <em>Conditional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Conditional</em>'.
   * @see org.xtext.example.mydsl.scheme.Conditional
   * @generated
   */
  EClass getConditional();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.scheme.Conditional#getCond <em>Cond</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cond</em>'.
   * @see org.xtext.example.mydsl.scheme.Conditional#getCond()
   * @see #getConditional()
   * @generated
   */
  EReference getConditional_Cond();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.scheme.Conditional#getTruthy <em>Truthy</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Truthy</em>'.
   * @see org.xtext.example.mydsl.scheme.Conditional#getTruthy()
   * @see #getConditional()
   * @generated
   */
  EReference getConditional_Truthy();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.scheme.Conditional#getFalsy <em>Falsy</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Falsy</em>'.
   * @see org.xtext.example.mydsl.scheme.Conditional#getFalsy()
   * @see #getConditional()
   * @generated
   */
  EReference getConditional_Falsy();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Print <em>Print</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Print</em>'.
   * @see org.xtext.example.mydsl.scheme.Print
   * @generated
   */
  EClass getPrint();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Print#getString <em>String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>String</em>'.
   * @see org.xtext.example.mydsl.scheme.Print#getString()
   * @see #getPrint()
   * @generated
   */
  EAttribute getPrint_String();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Lista <em>Lista</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lista</em>'.
   * @see org.xtext.example.mydsl.scheme.Lista
   * @generated
   */
  EClass getLista();

  /**
   * Returns the meta object for the attribute list '{@link org.xtext.example.mydsl.scheme.Lista#getNumeros <em>Numeros</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Numeros</em>'.
   * @see org.xtext.example.mydsl.scheme.Lista#getNumeros()
   * @see #getLista()
   * @generated
   */
  EAttribute getLista_Numeros();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.scheme.Any <em>Any</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Any</em>'.
   * @see org.xtext.example.mydsl.scheme.Any
   * @generated
   */
  EClass getAny();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Any#getAnyID <em>Any ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Any ID</em>'.
   * @see org.xtext.example.mydsl.scheme.Any#getAnyID()
   * @see #getAny()
   * @generated
   */
  EAttribute getAny_AnyID();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Any#getAnyInt <em>Any Int</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Any Int</em>'.
   * @see org.xtext.example.mydsl.scheme.Any#getAnyInt()
   * @see #getAny()
   * @generated
   */
  EAttribute getAny_AnyInt();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.scheme.Any#getAnyString <em>Any String</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Any String</em>'.
   * @see org.xtext.example.mydsl.scheme.Any#getAnyString()
   * @see #getAny()
   * @generated
   */
  EAttribute getAny_AnyString();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.scheme.Any#getAnyDefine <em>Any Define</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Any Define</em>'.
   * @see org.xtext.example.mydsl.scheme.Any#getAnyDefine()
   * @see #getAny()
   * @generated
   */
  EReference getAny_AnyDefine();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.scheme.Any#getAnyLambda <em>Any Lambda</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Any Lambda</em>'.
   * @see org.xtext.example.mydsl.scheme.Any#getAnyLambda()
   * @see #getAny()
   * @generated
   */
  EReference getAny_AnyLambda();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.scheme.Any#getAnyOperation <em>Any Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Any Operation</em>'.
   * @see org.xtext.example.mydsl.scheme.Any#getAnyOperation()
   * @see #getAny()
   * @generated
   */
  EReference getAny_AnyOperation();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.scheme.Any#getAnyConditional <em>Any Conditional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Any Conditional</em>'.
   * @see org.xtext.example.mydsl.scheme.Any#getAnyConditional()
   * @see #getAny()
   * @generated
   */
  EReference getAny_AnyConditional();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SchemeFactory getSchemeFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.ModelImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__COMMANDS = eINSTANCE.getModel_Commands();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.CommandImpl <em>Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.CommandImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getCommand()
     * @generated
     */
    EClass COMMAND = eINSTANCE.getCommand();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.DefineImpl <em>Define</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.DefineImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getDefine()
     * @generated
     */
    EClass DEFINE = eINSTANCE.getDefine();

    /**
     * The meta object literal for the '<em><b>Var Um</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DEFINE__VAR_UM = eINSTANCE.getDefine_VarUm();

    /**
     * The meta object literal for the '<em><b>Var Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DEFINE__VAR_VALUE = eINSTANCE.getDefine_VarValue();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.LambdaImpl <em>Lambda</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.LambdaImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getLambda()
     * @generated
     */
    EClass LAMBDA = eINSTANCE.getLambda();

    /**
     * The meta object literal for the '<em><b>Param</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAMBDA__PARAM = eINSTANCE.getLambda_Param();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.OperationImpl <em>Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.OperationImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getOperation()
     * @generated
     */
    EClass OPERATION = eINSTANCE.getOperation();

    /**
     * The meta object literal for the '<em><b>Arit op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPERATION__ARIT_OP = eINSTANCE.getOperation_Arit_op();

    /**
     * The meta object literal for the '<em><b>Log op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPERATION__LOG_OP = eINSTANCE.getOperation_Log_op();

    /**
     * The meta object literal for the '<em><b>Rel op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPERATION__REL_OP = eINSTANCE.getOperation_Rel_op();

    /**
     * The meta object literal for the '<em><b>Value Um</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPERATION__VALUE_UM = eINSTANCE.getOperation_ValueUm();

    /**
     * The meta object literal for the '<em><b>Value Dois</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPERATION__VALUE_DOIS = eINSTANCE.getOperation_ValueDois();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.ConditionalImpl <em>Conditional</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.ConditionalImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getConditional()
     * @generated
     */
    EClass CONDITIONAL = eINSTANCE.getConditional();

    /**
     * The meta object literal for the '<em><b>Cond</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL__COND = eINSTANCE.getConditional_Cond();

    /**
     * The meta object literal for the '<em><b>Truthy</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL__TRUTHY = eINSTANCE.getConditional_Truthy();

    /**
     * The meta object literal for the '<em><b>Falsy</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL__FALSY = eINSTANCE.getConditional_Falsy();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.PrintImpl <em>Print</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.PrintImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getPrint()
     * @generated
     */
    EClass PRINT = eINSTANCE.getPrint();

    /**
     * The meta object literal for the '<em><b>String</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRINT__STRING = eINSTANCE.getPrint_String();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.ListaImpl <em>Lista</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.ListaImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getLista()
     * @generated
     */
    EClass LISTA = eINSTANCE.getLista();

    /**
     * The meta object literal for the '<em><b>Numeros</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LISTA__NUMEROS = eINSTANCE.getLista_Numeros();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.scheme.impl.AnyImpl <em>Any</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.scheme.impl.AnyImpl
     * @see org.xtext.example.mydsl.scheme.impl.SchemePackageImpl#getAny()
     * @generated
     */
    EClass ANY = eINSTANCE.getAny();

    /**
     * The meta object literal for the '<em><b>Any ID</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ANY__ANY_ID = eINSTANCE.getAny_AnyID();

    /**
     * The meta object literal for the '<em><b>Any Int</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ANY__ANY_INT = eINSTANCE.getAny_AnyInt();

    /**
     * The meta object literal for the '<em><b>Any String</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ANY__ANY_STRING = eINSTANCE.getAny_AnyString();

    /**
     * The meta object literal for the '<em><b>Any Define</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ANY__ANY_DEFINE = eINSTANCE.getAny_AnyDefine();

    /**
     * The meta object literal for the '<em><b>Any Lambda</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ANY__ANY_LAMBDA = eINSTANCE.getAny_AnyLambda();

    /**
     * The meta object literal for the '<em><b>Any Operation</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ANY__ANY_OPERATION = eINSTANCE.getAny_AnyOperation();

    /**
     * The meta object literal for the '<em><b>Any Conditional</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ANY__ANY_CONDITIONAL = eINSTANCE.getAny_AnyConditional();

  }

} //SchemePackage
