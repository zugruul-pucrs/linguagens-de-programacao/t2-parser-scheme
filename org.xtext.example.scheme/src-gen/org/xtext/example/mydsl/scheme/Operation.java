/**
 * generated by Xtext 2.17.0
 */
package org.xtext.example.mydsl.scheme;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.scheme.Operation#getArit_op <em>Arit op</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.scheme.Operation#getLog_op <em>Log op</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.scheme.Operation#getRel_op <em>Rel op</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.scheme.Operation#getValueUm <em>Value Um</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.scheme.Operation#getValueDois <em>Value Dois</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.mydsl.scheme.SchemePackage#getOperation()
 * @model
 * @generated
 */
public interface Operation extends Command
{
  /**
   * Returns the value of the '<em><b>Arit op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arit op</em>' attribute.
   * @see #setArit_op(String)
   * @see org.xtext.example.mydsl.scheme.SchemePackage#getOperation_Arit_op()
   * @model
   * @generated
   */
  String getArit_op();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.scheme.Operation#getArit_op <em>Arit op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arit op</em>' attribute.
   * @see #getArit_op()
   * @generated
   */
  void setArit_op(String value);

  /**
   * Returns the value of the '<em><b>Log op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Log op</em>' attribute.
   * @see #setLog_op(String)
   * @see org.xtext.example.mydsl.scheme.SchemePackage#getOperation_Log_op()
   * @model
   * @generated
   */
  String getLog_op();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.scheme.Operation#getLog_op <em>Log op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Log op</em>' attribute.
   * @see #getLog_op()
   * @generated
   */
  void setLog_op(String value);

  /**
   * Returns the value of the '<em><b>Rel op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rel op</em>' attribute.
   * @see #setRel_op(String)
   * @see org.xtext.example.mydsl.scheme.SchemePackage#getOperation_Rel_op()
   * @model
   * @generated
   */
  String getRel_op();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.scheme.Operation#getRel_op <em>Rel op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Rel op</em>' attribute.
   * @see #getRel_op()
   * @generated
   */
  void setRel_op(String value);

  /**
   * Returns the value of the '<em><b>Value Um</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value Um</em>' attribute.
   * @see #setValueUm(int)
   * @see org.xtext.example.mydsl.scheme.SchemePackage#getOperation_ValueUm()
   * @model
   * @generated
   */
  int getValueUm();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.scheme.Operation#getValueUm <em>Value Um</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value Um</em>' attribute.
   * @see #getValueUm()
   * @generated
   */
  void setValueUm(int value);

  /**
   * Returns the value of the '<em><b>Value Dois</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value Dois</em>' attribute.
   * @see #setValueDois(int)
   * @see org.xtext.example.mydsl.scheme.SchemePackage#getOperation_ValueDois()
   * @model
   * @generated
   */
  int getValueDois();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.scheme.Operation#getValueDois <em>Value Dois</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value Dois</em>' attribute.
   * @see #getValueDois()
   * @generated
   */
  void setValueDois(int value);

} // Operation
