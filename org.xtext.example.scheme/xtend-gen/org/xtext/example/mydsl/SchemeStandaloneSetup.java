/**
 * generated by Xtext 2.17.0
 */
package org.xtext.example.mydsl;

import org.xtext.example.mydsl.SchemeStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
@SuppressWarnings("all")
public class SchemeStandaloneSetup extends SchemeStandaloneSetupGenerated {
  public static void doSetup() {
    new SchemeStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
}
