package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSchemeLexer extends Lexer {
    public static final int RULE_STRING=4;
    public static final int RULE_RELATIONAL_OPERATORS=8;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int RULE_LOGICAL_OPERATORS=7;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=9;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=10;
    public static final int RULE_ARITHMETIC_OPERATORS=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalSchemeLexer() {;} 
    public InternalSchemeLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalSchemeLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalScheme.g"; }

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:11:7: ( '(' )
            // InternalScheme.g:11:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:12:7: ( 'define' )
            // InternalScheme.g:12:9: 'define'
            {
            match("define"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:13:7: ( ')' )
            // InternalScheme.g:13:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:14:7: ( 'lambda' )
            // InternalScheme.g:14:9: 'lambda'
            {
            match("lambda"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:15:7: ( 'if' )
            // InternalScheme.g:15:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:16:7: ( '(print' )
            // InternalScheme.g:16:9: '(print'
            {
            match("(print"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:17:7: ( 'arr' )
            // InternalScheme.g:17:9: 'arr'
            {
            match("arr"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:18:7: ( 'vector ' )
            // InternalScheme.g:18:9: 'vector '
            {
            match("vector "); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "RULE_ARITHMETIC_OPERATORS"
    public final void mRULE_ARITHMETIC_OPERATORS() throws RecognitionException {
        try {
            int _type = RULE_ARITHMETIC_OPERATORS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1620:27: ( ( '+' | '-' | '*' | '/' | 'abs' | 'sqrt' ) )
            // InternalScheme.g:1620:29: ( '+' | '-' | '*' | '/' | 'abs' | 'sqrt' )
            {
            // InternalScheme.g:1620:29: ( '+' | '-' | '*' | '/' | 'abs' | 'sqrt' )
            int alt1=6;
            switch ( input.LA(1) ) {
            case '+':
                {
                alt1=1;
                }
                break;
            case '-':
                {
                alt1=2;
                }
                break;
            case '*':
                {
                alt1=3;
                }
                break;
            case '/':
                {
                alt1=4;
                }
                break;
            case 'a':
                {
                alt1=5;
                }
                break;
            case 's':
                {
                alt1=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalScheme.g:1620:30: '+'
                    {
                    match('+'); 

                    }
                    break;
                case 2 :
                    // InternalScheme.g:1620:34: '-'
                    {
                    match('-'); 

                    }
                    break;
                case 3 :
                    // InternalScheme.g:1620:38: '*'
                    {
                    match('*'); 

                    }
                    break;
                case 4 :
                    // InternalScheme.g:1620:42: '/'
                    {
                    match('/'); 

                    }
                    break;
                case 5 :
                    // InternalScheme.g:1620:46: 'abs'
                    {
                    match("abs"); 


                    }
                    break;
                case 6 :
                    // InternalScheme.g:1620:52: 'sqrt'
                    {
                    match("sqrt"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ARITHMETIC_OPERATORS"

    // $ANTLR start "RULE_LOGICAL_OPERATORS"
    public final void mRULE_LOGICAL_OPERATORS() throws RecognitionException {
        try {
            int _type = RULE_LOGICAL_OPERATORS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1622:24: ( ( 'and' | 'or' | 'not' ) )
            // InternalScheme.g:1622:26: ( 'and' | 'or' | 'not' )
            {
            // InternalScheme.g:1622:26: ( 'and' | 'or' | 'not' )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 'a':
                {
                alt2=1;
                }
                break;
            case 'o':
                {
                alt2=2;
                }
                break;
            case 'n':
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalScheme.g:1622:27: 'and'
                    {
                    match("and"); 


                    }
                    break;
                case 2 :
                    // InternalScheme.g:1622:33: 'or'
                    {
                    match("or"); 


                    }
                    break;
                case 3 :
                    // InternalScheme.g:1622:38: 'not'
                    {
                    match("not"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LOGICAL_OPERATORS"

    // $ANTLR start "RULE_RELATIONAL_OPERATORS"
    public final void mRULE_RELATIONAL_OPERATORS() throws RecognitionException {
        try {
            int _type = RULE_RELATIONAL_OPERATORS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1624:27: ( ( '>' | '<' | '=' | '>=' | '=<' ) )
            // InternalScheme.g:1624:29: ( '>' | '<' | '=' | '>=' | '=<' )
            {
            // InternalScheme.g:1624:29: ( '>' | '<' | '=' | '>=' | '=<' )
            int alt3=5;
            switch ( input.LA(1) ) {
            case '>':
                {
                int LA3_1 = input.LA(2);

                if ( (LA3_1=='=') ) {
                    alt3=4;
                }
                else {
                    alt3=1;}
                }
                break;
            case '<':
                {
                alt3=2;
                }
                break;
            case '=':
                {
                int LA3_3 = input.LA(2);

                if ( (LA3_3=='<') ) {
                    alt3=5;
                }
                else {
                    alt3=3;}
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalScheme.g:1624:30: '>'
                    {
                    match('>'); 

                    }
                    break;
                case 2 :
                    // InternalScheme.g:1624:34: '<'
                    {
                    match('<'); 

                    }
                    break;
                case 3 :
                    // InternalScheme.g:1624:38: '='
                    {
                    match('='); 

                    }
                    break;
                case 4 :
                    // InternalScheme.g:1624:42: '>='
                    {
                    match(">="); 


                    }
                    break;
                case 5 :
                    // InternalScheme.g:1624:47: '=<'
                    {
                    match("=<"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RELATIONAL_OPERATORS"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1626:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalScheme.g:1626:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalScheme.g:1626:11: ( '^' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='^') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalScheme.g:1626:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalScheme.g:1626:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')||(LA5_0>='A' && LA5_0<='Z')||LA5_0=='_'||(LA5_0>='a' && LA5_0<='z')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalScheme.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1628:10: ( ( '0' .. '9' )+ )
            // InternalScheme.g:1628:12: ( '0' .. '9' )+
            {
            // InternalScheme.g:1628:12: ( '0' .. '9' )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalScheme.g:1628:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1630:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalScheme.g:1630:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalScheme.g:1630:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='\"') ) {
                alt9=1;
            }
            else if ( (LA9_0=='\'') ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalScheme.g:1630:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalScheme.g:1630:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop7:
                    do {
                        int alt7=3;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0=='\\') ) {
                            alt7=1;
                        }
                        else if ( ((LA7_0>='\u0000' && LA7_0<='!')||(LA7_0>='#' && LA7_0<='[')||(LA7_0>=']' && LA7_0<='\uFFFF')) ) {
                            alt7=2;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalScheme.g:1630:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalScheme.g:1630:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalScheme.g:1630:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalScheme.g:1630:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop8:
                    do {
                        int alt8=3;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\\') ) {
                            alt8=1;
                        }
                        else if ( ((LA8_0>='\u0000' && LA8_0<='&')||(LA8_0>='(' && LA8_0<='[')||(LA8_0>=']' && LA8_0<='\uFFFF')) ) {
                            alt8=2;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalScheme.g:1630:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalScheme.g:1630:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1632:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalScheme.g:1632:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalScheme.g:1632:24: ( options {greedy=false; } : . )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0=='*') ) {
                    int LA10_1 = input.LA(2);

                    if ( (LA10_1=='/') ) {
                        alt10=2;
                    }
                    else if ( ((LA10_1>='\u0000' && LA10_1<='.')||(LA10_1>='0' && LA10_1<='\uFFFF')) ) {
                        alt10=1;
                    }


                }
                else if ( ((LA10_0>='\u0000' && LA10_0<=')')||(LA10_0>='+' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalScheme.g:1632:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1634:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalScheme.g:1634:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalScheme.g:1634:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='\u0000' && LA11_0<='\t')||(LA11_0>='\u000B' && LA11_0<='\f')||(LA11_0>='\u000E' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalScheme.g:1634:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // InternalScheme.g:1634:40: ( ( '\\r' )? '\\n' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='\n'||LA13_0=='\r') ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalScheme.g:1634:41: ( '\\r' )? '\\n'
                    {
                    // InternalScheme.g:1634:41: ( '\\r' )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0=='\r') ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // InternalScheme.g:1634:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1636:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalScheme.g:1636:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalScheme.g:1636:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt14=0;
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>='\t' && LA14_0<='\n')||LA14_0=='\r'||LA14_0==' ') ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalScheme.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalScheme.g:1638:16: ( . )
            // InternalScheme.g:1638:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalScheme.g:1:8: ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | RULE_ARITHMETIC_OPERATORS | RULE_LOGICAL_OPERATORS | RULE_RELATIONAL_OPERATORS | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt15=18;
        alt15 = dfa15.predict(input);
        switch (alt15) {
            case 1 :
                // InternalScheme.g:1:10: T__14
                {
                mT__14(); 

                }
                break;
            case 2 :
                // InternalScheme.g:1:16: T__15
                {
                mT__15(); 

                }
                break;
            case 3 :
                // InternalScheme.g:1:22: T__16
                {
                mT__16(); 

                }
                break;
            case 4 :
                // InternalScheme.g:1:28: T__17
                {
                mT__17(); 

                }
                break;
            case 5 :
                // InternalScheme.g:1:34: T__18
                {
                mT__18(); 

                }
                break;
            case 6 :
                // InternalScheme.g:1:40: T__19
                {
                mT__19(); 

                }
                break;
            case 7 :
                // InternalScheme.g:1:46: T__20
                {
                mT__20(); 

                }
                break;
            case 8 :
                // InternalScheme.g:1:52: T__21
                {
                mT__21(); 

                }
                break;
            case 9 :
                // InternalScheme.g:1:58: RULE_ARITHMETIC_OPERATORS
                {
                mRULE_ARITHMETIC_OPERATORS(); 

                }
                break;
            case 10 :
                // InternalScheme.g:1:84: RULE_LOGICAL_OPERATORS
                {
                mRULE_LOGICAL_OPERATORS(); 

                }
                break;
            case 11 :
                // InternalScheme.g:1:107: RULE_RELATIONAL_OPERATORS
                {
                mRULE_RELATIONAL_OPERATORS(); 

                }
                break;
            case 12 :
                // InternalScheme.g:1:133: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 13 :
                // InternalScheme.g:1:141: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 14 :
                // InternalScheme.g:1:150: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 15 :
                // InternalScheme.g:1:162: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 16 :
                // InternalScheme.g:1:178: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 17 :
                // InternalScheme.g:1:194: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 18 :
                // InternalScheme.g:1:202: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA15 dfa15 = new DFA15(this);
    static final String DFA15_eotS =
        "\1\uffff\1\32\1\34\1\uffff\4\34\3\uffff\1\44\3\34\3\uffff\1\30\2\uffff\2\30\4\uffff\1\34\2\uffff\1\34\1\60\4\34\3\uffff\1\34\1\66\1\34\4\uffff\2\34\1\uffff\1\72\1\44\1\66\2\34\1\uffff\1\66\2\34\1\uffff\1\34\1\44\3\34\1\103\1\104\1\34\3\uffff";
    static final String DFA15_eofS =
        "\106\uffff";
    static final String DFA15_minS =
        "\1\0\1\160\1\145\1\uffff\1\141\1\146\1\142\1\145\3\uffff\1\52\1\161\1\162\1\157\3\uffff\1\101\2\uffff\2\0\4\uffff\1\146\2\uffff\1\155\1\60\1\162\1\163\1\144\1\143\3\uffff\1\162\1\60\1\164\4\uffff\1\151\1\142\1\uffff\3\60\2\164\1\uffff\1\60\1\156\1\144\1\uffff\1\157\1\60\1\145\1\141\1\162\2\60\1\40\3\uffff";
    static final String DFA15_maxS =
        "\1\uffff\1\160\1\145\1\uffff\1\141\1\146\1\162\1\145\3\uffff\1\57\1\161\1\162\1\157\3\uffff\1\172\2\uffff\2\uffff\4\uffff\1\146\2\uffff\1\155\1\172\1\162\1\163\1\144\1\143\3\uffff\1\162\1\172\1\164\4\uffff\1\151\1\142\1\uffff\3\172\2\164\1\uffff\1\172\1\156\1\144\1\uffff\1\157\1\172\1\145\1\141\1\162\2\172\1\40\3\uffff";
    static final String DFA15_acceptS =
        "\3\uffff\1\3\4\uffff\3\11\4\uffff\3\13\1\uffff\1\14\1\15\2\uffff\1\21\1\22\1\6\1\1\1\uffff\1\14\1\3\6\uffff\1\11\1\17\1\20\3\uffff\1\13\1\15\1\16\1\21\2\uffff\1\5\5\uffff\1\12\3\uffff\1\7\10\uffff\1\2\1\4\1\10";
    static final String DFA15_specialS =
        "\1\0\24\uffff\1\1\1\2\57\uffff}>";
    static final String[] DFA15_transitionS = {
            "\11\30\2\27\2\30\1\27\22\30\1\27\1\30\1\25\4\30\1\26\1\1\1\3\1\12\1\10\1\30\1\11\1\30\1\13\12\24\2\30\1\20\1\21\1\17\2\30\32\23\3\30\1\22\1\23\1\30\1\6\2\23\1\2\4\23\1\5\2\23\1\4\1\23\1\16\1\15\3\23\1\14\2\23\1\7\4\23\uff85\30",
            "\1\31",
            "\1\33",
            "",
            "\1\36",
            "\1\37",
            "\1\41\13\uffff\1\42\3\uffff\1\40",
            "\1\43",
            "",
            "",
            "",
            "\1\45\4\uffff\1\46",
            "\1\47",
            "\1\50",
            "\1\51",
            "",
            "",
            "",
            "\32\34\4\uffff\1\34\1\uffff\32\34",
            "",
            "",
            "\0\54",
            "\0\54",
            "",
            "",
            "",
            "",
            "\1\56",
            "",
            "",
            "\1\57",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\61",
            "\1\62",
            "\1\63",
            "\1\64",
            "",
            "",
            "",
            "\1\65",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\67",
            "",
            "",
            "",
            "",
            "\1\70",
            "\1\71",
            "",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\73",
            "\1\74",
            "",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\75",
            "\1\76",
            "",
            "\1\77",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\100",
            "\1\101",
            "\1\102",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\1\105",
            "",
            "",
            ""
    };

    static final short[] DFA15_eot = DFA.unpackEncodedString(DFA15_eotS);
    static final short[] DFA15_eof = DFA.unpackEncodedString(DFA15_eofS);
    static final char[] DFA15_min = DFA.unpackEncodedStringToUnsignedChars(DFA15_minS);
    static final char[] DFA15_max = DFA.unpackEncodedStringToUnsignedChars(DFA15_maxS);
    static final short[] DFA15_accept = DFA.unpackEncodedString(DFA15_acceptS);
    static final short[] DFA15_special = DFA.unpackEncodedString(DFA15_specialS);
    static final short[][] DFA15_transition;

    static {
        int numStates = DFA15_transitionS.length;
        DFA15_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA15_transition[i] = DFA.unpackEncodedString(DFA15_transitionS[i]);
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | RULE_ARITHMETIC_OPERATORS | RULE_LOGICAL_OPERATORS | RULE_RELATIONAL_OPERATORS | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA15_0 = input.LA(1);

                        s = -1;
                        if ( (LA15_0=='(') ) {s = 1;}

                        else if ( (LA15_0=='d') ) {s = 2;}

                        else if ( (LA15_0==')') ) {s = 3;}

                        else if ( (LA15_0=='l') ) {s = 4;}

                        else if ( (LA15_0=='i') ) {s = 5;}

                        else if ( (LA15_0=='a') ) {s = 6;}

                        else if ( (LA15_0=='v') ) {s = 7;}

                        else if ( (LA15_0=='+') ) {s = 8;}

                        else if ( (LA15_0=='-') ) {s = 9;}

                        else if ( (LA15_0=='*') ) {s = 10;}

                        else if ( (LA15_0=='/') ) {s = 11;}

                        else if ( (LA15_0=='s') ) {s = 12;}

                        else if ( (LA15_0=='o') ) {s = 13;}

                        else if ( (LA15_0=='n') ) {s = 14;}

                        else if ( (LA15_0=='>') ) {s = 15;}

                        else if ( (LA15_0=='<') ) {s = 16;}

                        else if ( (LA15_0=='=') ) {s = 17;}

                        else if ( (LA15_0=='^') ) {s = 18;}

                        else if ( ((LA15_0>='A' && LA15_0<='Z')||LA15_0=='_'||(LA15_0>='b' && LA15_0<='c')||(LA15_0>='e' && LA15_0<='h')||(LA15_0>='j' && LA15_0<='k')||LA15_0=='m'||(LA15_0>='p' && LA15_0<='r')||(LA15_0>='t' && LA15_0<='u')||(LA15_0>='w' && LA15_0<='z')) ) {s = 19;}

                        else if ( ((LA15_0>='0' && LA15_0<='9')) ) {s = 20;}

                        else if ( (LA15_0=='\"') ) {s = 21;}

                        else if ( (LA15_0=='\'') ) {s = 22;}

                        else if ( ((LA15_0>='\t' && LA15_0<='\n')||LA15_0=='\r'||LA15_0==' ') ) {s = 23;}

                        else if ( ((LA15_0>='\u0000' && LA15_0<='\b')||(LA15_0>='\u000B' && LA15_0<='\f')||(LA15_0>='\u000E' && LA15_0<='\u001F')||LA15_0=='!'||(LA15_0>='#' && LA15_0<='&')||LA15_0==','||LA15_0=='.'||(LA15_0>=':' && LA15_0<=';')||(LA15_0>='?' && LA15_0<='@')||(LA15_0>='[' && LA15_0<=']')||LA15_0=='`'||(LA15_0>='{' && LA15_0<='\uFFFF')) ) {s = 24;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA15_21 = input.LA(1);

                        s = -1;
                        if ( ((LA15_21>='\u0000' && LA15_21<='\uFFFF')) ) {s = 44;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA15_22 = input.LA(1);

                        s = -1;
                        if ( ((LA15_22>='\u0000' && LA15_22<='\uFFFF')) ) {s = 44;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 15, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}